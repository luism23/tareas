const express = require('express')
const app = require('../app')
const router = require('express').Router()
const Task = require('../models/Task')
const {save} = require('../database')


router.get('/', async (req,res) => {

    const tasks = await Task.find()


    res.render("index" ,{tasks: tasks})
})

router.post('/tasks/add', async (req,res) => {

    const task = Task(req.body)
    console.log(task);
    const result = await save({collection:"test",data:task})
    console.log(result);
    res.redirect("/")
//    const taskSaved = await task.saved()
//    console.log(taskSaved)
})

router.get('/abouts', (req,res) => {
    res.render('abouts')
})

module.exports = router
