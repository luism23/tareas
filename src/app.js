const express = require("express")
const exphbs = require("exphbs")
const IndexRoutes = require("./routes/index.routes")
const path = require("path")
const morgan = require("morgan")


const app = express()

app.set('views', path.join(__dirname, 'views'))

app.engine('hbs', require('exphbs',({
    layoutsDir: path.join(app.get('views'),'layouts'),
    defaultLayout : "main",
    extname: ".hbs"

})))
app.set('view engine', 'hbs')

//middlewares
app.use(morgan('dev'))
app.use(express.urlencoded({extended : false}))

//Routes
app.use("/",IndexRoutes)

module.exports = app