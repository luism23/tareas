const env = require('./env')
const { connect } = require("mongoose")
const { MongoClient, ObjectId } = require('mongodb')


const connectmongo = async () => {
    try {
        const db = await connect(env.URLDATABASE)
        console.log('DB connected to', db.connection.name)
        return db
    } catch (error) {
        console.log(error);
        return null
    }
}
const save = async ({ collection, data, options = {} }) => {
    const client = new MongoClient(env.URLDATABASE)
    try {
        if (!client) {
            throw new Error('error conection database Mongo')
        }
        await client.connect()
        const dbMongo = client.db(env.DATABASE_MONGO)
        const coll = dbMongo.collection(collection)
        const response = await coll.insert(data, options)
        const result = await response
        return result
    } catch (error) {
        return {
            type: 'error',
            error: `${error}`
        }
    } finally {
        await client.close()
    }

}
module.exports = {
    save
}